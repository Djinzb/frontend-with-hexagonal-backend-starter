resource "aws_s3_bucket" "terraform_bucket" {
  bucket = "dpg-${local.application_tag}-terraform"

  tags = merge(local.common_tags, {
    "Type" = "S3"
  })
}