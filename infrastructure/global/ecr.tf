resource "aws_ecr_repository" "ecr_repository" {
  name                 = "${local.application_tag}-tf"
  image_tag_mutability = "IMMUTABLE"
  image_scanning_configuration {
    scan_on_push = true
  }
}

resource "aws_ecr_lifecycle_policy" "ecr_repository_policy" {
  repository = aws_ecr_repository.ecr_repository.name

  policy = jsonencode({
    "rules" : [
      {
        rulePriority : 1,
        description : "Keep no more than 100 images",
        selection : {
          "tagStatus" : "any",
          "countType" : "imageCountMoreThan",
          "countNumber" : 100
        },
        action : {
          "type" : "expire"
        }
      }
    ]
  })
}
