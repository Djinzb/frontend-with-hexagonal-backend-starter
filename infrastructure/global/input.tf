data "aws_caller_identity" "current" {}

locals {
  squad           = "Ink"
  application     = "keywords-archive"
  application_tag = "keywords-archive"
  source_branch   = "master"
  region          = "eu-west-1"
  account_id      = data.aws_caller_identity.current.account_id
  stage           = terraform.workspace
  environment     = terraform.workspace
  origin          = "Terraform"

  application_repository = "${local.application_tag}-tf"

  terraform_workspaces = {
    "0" = "nonprod"
    "1" = "prod"
  }

  common_tags = {
    Squad       = local.squad
    Application = local.application_tag
    Origin      = local.origin
  }
}
