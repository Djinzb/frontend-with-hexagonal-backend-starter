resource "aws_iam_role" "keywords_codebuild_role" {
  name                = "${local.squad}-${local.application_tag}-codebuild-role-tf"
  assume_role_policy  = data.aws_iam_policy_document.keywords_codebuild_assume_role_policy_document.json
  managed_policy_arns = [
    aws_iam_policy.keywords_codebuild_policy.arn
  ]
}

data aws_iam_policy_document "keywords_codebuild_assume_role_policy_document" {
  statement {

    principals {
      identifiers = ["codebuild.amazonaws.com"]
      type        = "Service"
    }
    actions = ["sts:AssumeRole"]
    effect  = "Allow"
  }
}

resource "aws_iam_policy" "keywords_codebuild_policy" {
  name   = "${local.application_tag}-codebuild-policy-tf"
  policy = data.aws_iam_policy_document.keywords_codebuild_policy_document_tf.json
}

data "aws_iam_policy_document" "keywords_codebuild_policy_document_tf" {

  statement {
    actions = [
      "logs:CreateLogGroup",
      "logs:CreateLogStream",
      "logs:DescribeLogGroups",
      "logs:DescribeLogStreams",
      "logs:GetLogEvents",
      "logs:PutLogEvents",
      "logs:FilterLogEvents",
      "logs:TagLogGroup",
      "logs:PutRetentionPolicy",
      "logs:DeleteLogGroup",
    ]
    resources = [
      "arn:aws:logs:${local.region}:${local.account_id}:log-group:/aws/codebuild/${local.application_tag}-codebuild-tf",
      "arn:aws:logs:${local.region}:${local.account_id}:log-group:/aws/codebuild/${local.application_tag}-codebuild-tf:*",
    ]
  }

  statement {
    actions = [
      "s3:*"
    ]
    resources = [
      data.aws_s3_bucket.keywords_artifacts_bucket.arn,
      "${data.aws_s3_bucket.keywords_artifacts_bucket.arn}/*"
    ]
  }

  statement {
    actions = [
      "secretsmanager:DescribeSecret",
      "secretsmanager:GetSecretValue",
      "secretsmanager:GetResourcePolicy"
    ]
    resources = [
      "arn:aws:secretsmanager:${local.region}:${local.account_id}:secret:artifactory_npm*"
    ]
  }

  statement {
    actions = [
      "ecr:InitiateLayerUpload",
      "ecr:BatchCheckLayerAvailability",
      "ecr:CompleteLayerUpload",
      "ecr:PutImage",
      "ecr:UploadLayerPart"
    ]
    resources = [
      data.aws_ecr_repository.ecr_repository.arn
    ]
  }
  statement {
    actions = [
      "ecr:GetAuthorizationToken"
    ]
    resources = [
      "*"
    ]
  }
}
