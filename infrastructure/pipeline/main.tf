provider "aws" {
  region = "eu-west-1"
  default_tags {
    tags = local.common_tags
  }
}

terraform {
  backend "s3" {
    bucket = "dpg-keywords-archive-terraform"
    key = "pipeline/terraform.tfstate"
    region = "eu-west-1"
  }
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.0"
    }
  }
}
