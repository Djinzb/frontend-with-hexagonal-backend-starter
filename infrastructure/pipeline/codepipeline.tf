resource "aws_codepipeline" "keywords_pipeline" {
  name = "${local.application_tag}-pipeline-tf"

  role_arn = aws_iam_role.keywords_codepipeline_role.arn

  artifact_store {
    location = data.aws_s3_bucket.keywords_artifacts_bucket.id
    type     = "S3"
  }

  stage {
    name = "Source"
    action {

      category = "Source"
      name     = "source"
      owner    = "AWS"
      provider = "CodeStarSourceConnection"
      version  = "1"

      output_artifacts = ["source_output"]

      configuration = {
        ConnectionArn    = data.aws_codestarconnections_connection.keywords_bitbucket_connection.arn
        FullRepositoryId = "persgroep/keywords-archive"
        BranchName       = "master"
      }
    }
  }
  stage {
    name = "Build"
    action {
      run_order       = 1
      category        = "Build"
      name            = "build-keywords"
      owner           = "AWS"
      provider        = "CodeBuild"
      version         = "1"
      input_artifacts = [
        "source_output"
      ]
      configuration = {
        ProjectName = aws_codebuild_project.keywords_codebuild.name
      }
    }
  }

}
