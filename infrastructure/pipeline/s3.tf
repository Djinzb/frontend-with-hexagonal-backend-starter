data "aws_s3_bucket" "terraform_bucket" {
  bucket = "dpg-${local.application_tag}-terraform"
}

data "aws_s3_bucket" "keywords_artifacts_bucket" {
  bucket = "dpg-artifacts-${local.account_id}-${local.region}-cfn"
}
