resource "aws_iam_role" "keywords_codepipeline_role" {
  name               = "${local.application_tag}-codepipeline-role-tf"
  assume_role_policy = data.aws_iam_policy_document.keywords_codepipeline_assume_role_policy_document.json

  managed_policy_arns = [
    aws_iam_policy.keywords_codepipeline_policy.arn
  ]
}

data "aws_iam_policy_document" "keywords_codepipeline_assume_role_policy_document" {
  statement {
    actions = [
      "sts:AssumeRole"
    ]
    principals {
      identifiers = [
        "codepipeline.amazonaws.com"
      ]
      type = "Service"
    }
  }
}

resource "aws_iam_policy" "keywords_codepipeline_policy" {
  name   = "${local.application_tag}-codepipeline-policy-tf"
  policy = data.aws_iam_policy_document.codepipeline_policy_document.json
}

data "aws_iam_policy_document" "codepipeline_policy_document" {
  statement {
    effect  = "Allow"
    actions = [
      "codestar-connections:ListConnections",
      "codestar-connections:GetConnection",
      "codestar-connections:UseConnection",
      "codestar-connections:ListTagsForResource",
    ]
    resources = [
      data.aws_codestarconnections_connection.keywords_bitbucket_connection.arn
    ]
  }

  statement {
    effect  = "Allow"
    actions = [
      "s3:*"
    ]
    resources = [
      data.aws_s3_bucket.keywords_artifacts_bucket.arn,
      "${data.aws_s3_bucket.keywords_artifacts_bucket.arn}/*"
    ]
  }
  statement {
    actions = [
      "codebuild:StartBuild",
      "codebuild:BatchGetBuilds"
    ]
    resources = [
      aws_codebuild_project.keywords_codebuild.id,
    ]
  }
}
