resource "aws_codebuild_project" "keywords_codebuild" {
  name        = "${local.application_tag}-codebuild-tf"
  description = "CodeBuild project for the ts code for ${local.application}"
  artifacts {
    type = "CODEPIPELINE"
  }

  cache {
    type     = "S3"
    location = data.aws_s3_bucket.keywords_artifacts_bucket.bucket
  }

  environment {
    type            = "LINUX_CONTAINER"
    compute_type    = "BUILD_GENERAL1_MEDIUM"
    image           = "aws/codebuild/standard:6.0"
    privileged_mode = "true"
  }
  build_timeout = "15"
  service_role  = aws_iam_role.keywords_codebuild_role.arn

  source {
    type      = "CODEPIPELINE"
    buildspec = templatefile("buildspecs/build_buildspec.yml", {
      tf_ecr_url = data.aws_ecr_repository.ecr_repository.repository_url,
      tf_account_id = local.account_id
    } )
  }

  tags = {
    "Type" = "CodeBuild"
  }
}
