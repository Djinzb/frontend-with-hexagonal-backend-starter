import {Button, FormControl, FormLabel, Input} from "@dnr-ui/react";
import styles from './sidebar.module.scss';

const Index = () => {

    return (
        <aside className={styles.sidebar}>
            <h1>Trefwoorden Humo</h1>
            <SearchForm />
        </aside>
    )
}

const SearchForm = () => {
    return (
        <form onSubmit={(e) => e.preventDefault()}>
            <FormControl>
                <FormLabel htmlFor={'articleName'}>Artikelnaam:</FormLabel>
                <Input type={'search'} id={`articleName`} name={'articleName'} onChange={() => console.log('zoek artikel naam')} />
            </FormControl>
            <FormControl>
                <FormLabel htmlFor={'notsureyet'}>Humonummer:</FormLabel>
                <Input type={'number'} id={`notsureyet`} name={'notsureyet'} onChange={() => console.log('number')} />
            </FormControl>
            <Button extraClasses={['button--block']}>Zoek</Button>
        </form>
    )
}

export default Index
