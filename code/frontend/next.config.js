module.exports = {
  output: 'standalone',
  reactStrictMode: true,
  experimental: {
    externalDir: true
  }
}
