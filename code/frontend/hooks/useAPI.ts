import useSWR, { SWRResponse } from 'swr'
import {SearchDTO} from "../../shared/types";

export function useSearch(): SWRResponse {
    return useSWR<SearchDTO, Error>('/api/search', fetcher);
}

// General fetcher
const fetcher = async (
    input: RequestInfo,
    init: RequestInit,
    ...args: any[]
) => {
    const res = await fetch(input, init);
    return res.json();
};

