import type { NextApiRequest, NextApiResponse } from 'next'
import {SearchDTO} from "../../../shared/types";
import {SearchService} from "../../../backend/search-use-case/application/search.service";
import {SearchAdapter} from "../../../backend/search-use-case/application/adapters/search.adapter";

const searchService = new SearchService(new SearchAdapter);
export default function handler(
    req: NextApiRequest,
    res: NextApiResponse<SearchDTO>
) {
    const searchResult = searchService.find()
    res.status(200).json(searchResult)
}
