import Head from 'next/head'
import styles from '../styles/Home.module.scss'
import {useSearch} from "../hooks/useAPI";
import Sidebar from "../components/sidebar";

export default function Home() {
    const {data} = useSearch();

    return (
        <>
            <Head>
                <title>Trefwoorden</title>
                <meta name="description" content="Keywords archive"/>
                <meta name="viewport" content="width=device-width, initial-scale=1"/>
                <link rel="icon" href="/favicon.ico"/>
            </Head>
            <main className={styles.main}>
                <Sidebar />
                <section className={styles.content}><h1>Content</h1>
                <p>{data && data.message}</p>
                </section>
            </main>
        </>
    )
}
