import {SearchPort} from "./ports/search.port";
import {SearchDTO} from "../../../shared/types";

export class SearchService {
    private searchPort: SearchPort;

    constructor(searchPort: SearchPort) {
        this.searchPort = searchPort;
    }

    find(): SearchDTO {
        return this.searchPort.find()
    }
}
