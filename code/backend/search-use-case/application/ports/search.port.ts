import {SearchDTO} from "../../../../shared/types";

export interface SearchPort {
    find(): SearchDTO
}
